package http

import (
	"log"
	"net"
	"time"

	"gitlab.com/basriyasin/cosmart/config"
	"gitlab.com/basriyasin/cosmart/pkg/http"
	"gitlab.com/basriyasin/cosmart/service/http/book"
	"gitlab.com/basriyasin/cosmart/service/http/healthcheck"
	"gitlab.com/basriyasin/cosmart/service/http/schedule"
)

type server struct {
	*http.Service
	middleware *http.Middleware
}

const (
	tokenAuth = iota
)

func New(c *config.HTTP) *server {
	return &server{
		middleware: http.InitMiddeware(),
		Service: http.New(&http.Config{
			Address:      c.Address,
			WriteTimeout: time.Second * time.Duration(c.WriteTimeout),
			ReadTimeout:  time.Second * time.Duration(c.ReadTimeout),
		}),
	}
}

func (s *server) Run() {
	s.registerMiddleware()
	s.RegisterService(s)

	log.Printf("starting http server on %s", s.Address())
	listener, err := net.Listen("tcp4", s.Address())
	if err != nil {
		log.Fatalf("failed init http listener: %v", err)
	}

	err = s.Serve(listener)
	if err != nil {
		log.Fatalf("failed serve http: %v", err)
	}
}

func (s *server) registerMiddleware() {
	//
}

func (s *server) RegisterHandler(r *http.Router) {
	r.Get("/ping", healthcheck.Ping)
	r.Get("/list/{subject}", book.Search)

	r.Post("/pickup", schedule.Pickup)
	r.Get("/pickup", schedule.List)
}
