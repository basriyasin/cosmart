package book

import (
	"context"
	"fmt"

	"gitlab.com/basriyasin/cosmart/domain/author"
	"gitlab.com/basriyasin/cosmart/domain/book"
	"gitlab.com/basriyasin/cosmart/pkg/errors"
	"gitlab.com/basriyasin/cosmart/pkg/tracer"
	"gitlab.com/basriyasin/cosmart/pkg/util"
)

func (r repo) cache(ctx context.Context, key string, data interface{}) {
	span, _ := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	r.storage.Store(key, data)
}

func (r repo) getSubjectFromCache(ctx context.Context, key string) (res []book.Book, err error) {
	span, _ := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	data, ok := r.storage.Load(key)
	if !ok {
		return res, &errors.DataNotFound
	}

	res, ok = data.([]book.Book)
	if !ok {
		return res, &errors.InternalServerError
	}

	return
}

func (r repo) getSubjectFromCloud(ctx context.Context, key string) (res book.APIGetSubjectResponse, err error) {
	span, _ := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	err = r.api.Get(fmt.Sprintf(r.getBookDetailURL, key)).Call().Unmarshal(&res)
	return
}

func (r repo) GetBooks(ctx context.Context, subject string) (res []book.Book, err error) {
	span, ctx := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	res, err = r.getSubjectFromCache(ctx, subject)
	if (err == nil) ||
		(err != nil && err != &errors.DataNotFound) {
		return
	}

	data, err := r.getSubjectFromCloud(ctx, subject)
	if err != nil {
		return
	}

	for _, v := range data.Works {
		authors := []author.Author{}
		for _, vv := range v.Authors {
			authors = append(authors, author.Author{
				Key:  util.GetOpenLibraryKey(vv.Key),
				Name: vv.Name,
			})
		}
		res = append(res, book.Book{
			Key:      util.GetOpenLibraryKey(v.Key),
			Title:    v.Title,
			Subtitle: v.Subtitle,
			Authors:  authors,
		})
	}

	r.cache(ctx, subject, res)
	return
}
