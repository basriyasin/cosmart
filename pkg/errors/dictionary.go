package errors

var (
	// 40xxx
	BadRequest = Error{
		Status: 400,
		Code:   "40000",
		Reason: "invalid payload",
	}

	// 41xxx
	DataNotFound = Error{
		Status: 400,
		Code:   "41000",
		Reason: "data not found",
	}

	// 50xxx
	InternalServerError = Error{
		Status: 500,
		Code:   "40000",
		Reason: "sorry we had an unexpected error, please try again later",
	}
)
