package api

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"gitlab.com/basriyasin/cosmart/config"
)

type defaultClient struct {
	config  config.HTTP
	client  *http.Client
	req     *http.Request
	res     *http.Response
	resBody []byte
	err     error
}

func (c *defaultClient) AddHeader(key, val string) Client {
	c.req.Header.Add(key, val)
	return c
}

func (c *defaultClient) AddHeaders(header map[string]string) Client {
	for key, val := range header {
		c.AddHeader(key, val)
	}
	return c
}

func (c *defaultClient) Insecure() Client {
	c.client = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	return c
}

func (c *defaultClient) Call() Client {
	var err error
	defer func() {
		c.err = err
		if c.res != nil {
			c.res.Body.Close()
		}
	}()

	client := http.DefaultClient
	if c.client != nil {
		client = c.client
	}

	client.Timeout = time.Duration(c.config.ReadTimeout)
	c.res, err = client.Do(c.req)
	if err != nil {
		return c
	}

	c.resBody, err = ioutil.ReadAll(c.res.Body)
	if err != nil {
		return c
	}

	return c
}

func (c *defaultClient) Error() error {
	if c.err != nil {
		return fmt.Errorf("err: %v, with body: %s", c.err, c.resBody)
	}

	return nil
}

func (c *defaultClient) Response() (*http.Response, error) {
	return c.res, c.Error()
}

func (c *defaultClient) ResponseBody() ([]byte, error) {
	return c.resBody, c.Error()
}

func (c *defaultClient) Unmarshal(v interface{}) error {
	if c.err != nil {
		return c.Error()
	}

	c.err = json.Unmarshal(c.resBody, v)
	return c.Error()
}

func newClient(method, url string, body io.ReadCloser) (Client, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}

	return &defaultClient{
		req: req,
	}, nil
}

func newRequest(method, url string, body interface{}) (client Client, err error) {
	if body == nil {
		return newClient(method, url, nil)
	}

	b, err := json.Marshal(body)
	if err != nil {
		return client, err
	}

	return newClient(method, url, io.NopCloser(bytes.NewReader(b)))
}
