package api

import (
	"fmt"
	"net/http"
	"net/url"
)

type (
	Client interface {
		AddHeader(key, val string) Client
		AddHeaders(header map[string]string) Client
		Insecure() Client
		Call() Client
		Error() error
		Response() (*http.Response, error)
		ResponseBody() ([]byte, error)
		Unmarshal(v interface{}) error
	}

	APIClient interface {
		Get(url string) Client
		Post(url string, body interface{}) (Client, error)
		PostForm(url string, form url.Values) (Client, error)
		Put(url string, body interface{}) (Client, error)
		Patch(url string, body interface{}) (Client, error)
		Delete(url string, body interface{}) (Client, error)
		Do(client *http.Client, req *http.Request) (*http.Response, error)
	}

	client struct{}
)

func New() APIClient {
	return &client{}
}

func (c client) Get(url string) Client {
	client, _ := newRequest(http.MethodGet, url, nil)
	return client
}

func (c client) Post(url string, body interface{}) (Client, error) {
	return newRequest(http.MethodPost, url, body)
}

func (c client) PostForm(url string, form url.Values) (Client, error) {
	return newClient(http.MethodPost, fmt.Sprintf("%s?%s", url, form.Encode()), nil)
}

func (c client) Put(url string, body interface{}) (Client, error) {
	return newRequest(http.MethodPut, url, body)
}

func (c client) Patch(url string, body interface{}) (Client, error) {
	return newRequest(http.MethodPatch, url, body)
}

func (c client) Delete(url string, body interface{}) (Client, error) {
	return newRequest(http.MethodDelete, url, body)
}

func (c client) Do(client *http.Client, req *http.Request) (*http.Response, error) {
	if client == nil {
		client = http.DefaultClient
	}

	return client.Do(req)
}
