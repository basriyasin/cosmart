package schedule

import (
	"context"

	"gitlab.com/basriyasin/cosmart/domain/book"
)

type (
	ScheduleRepo interface {
		Insert(ctx context.Context, schedule Schedule) error
		List(ctx context.Context) ([]Schedule, error)
	}

	ScheduleUsecase interface {
		RequestPickup(ctx context.Context, req RequestPickup) error
		List(ctx context.Context) ([]Schedule, error)
	}

	Schedule struct {
		PickupTime string `json:"pickup_time"`
		book.Book
	}

	// Request
	RequestPickup struct {
		BookKey    string `json:"book_key"    val:"required"`
		PickupTime string `json:"pickup_time" val:"required,datetime"`
	}
)
