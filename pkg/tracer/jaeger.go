package tracer

import (
	"log"
	"time"

	"github.com/uber/jaeger-client-go"
	jaegercfg "github.com/uber/jaeger-client-go/config"
	jaegerlog "github.com/uber/jaeger-client-go/log"
	"github.com/uber/jaeger-lib/metrics"

	"gitlab.com/basriyasin/cosmart/pkg/env"
)

type Config struct {
	LocalAgentAddr string `yaml:"local_agent_addr" json:"local_agent_addr"`
	Enabled        bool   `yaml:"enabled"          json:"enabled"`
}

func InitJaeger(appName string, config Config) {
	log.Printf("Jeager Enabled: %v", config.Enabled)
	cfg := jaegercfg.Configuration{
		Disabled: !config.Enabled,
		Sampler: &jaegercfg.SamplerConfig{
			Type:  jaeger.SamplerTypeConst,
			Param: 1,
		},
		Reporter: &jaegercfg.ReporterConfig{
			LogSpans:            true,
			BufferFlushInterval: 1 * time.Second,
			LocalAgentHostPort:  config.LocalAgentAddr,
		},
	}

	if appName == "" {
		appName = env.DefaultAppName
	}

	// Initialize tracer with a logger and a metrics factory, returning closer
	_, err := cfg.InitGlobalTracer(
		appName+"-"+env.ServiceEnv(),
		jaegercfg.Logger(jaegerlog.NullLogger),
		jaegercfg.Metrics(metrics.NullFactory),
	)
	if err != nil {
		log.Fatalf("Could not initialize jaeger tracer: %s", err.Error())
		return
	}
}
