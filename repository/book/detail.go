package book

import (
	"context"
	"fmt"

	"gitlab.com/basriyasin/cosmart/domain/author"
	"gitlab.com/basriyasin/cosmart/domain/book"
	"gitlab.com/basriyasin/cosmart/pkg/errors"
	"gitlab.com/basriyasin/cosmart/pkg/tracer"
)

func (r repo) getBookDetailFromCache(ctx context.Context, key string) (res book.Book, err error) {
	span, _ := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	data, ok := r.storage.Load(key)
	if !ok {
		return res, &errors.DataNotFound
	}

	res, ok = data.(book.Book)
	if !ok {
		return res, &errors.InternalServerError
	}

	return
}

func (r repo) getBookDetailFromCloud(ctx context.Context, key string) (res book.Book, err error) {
	span, _ := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	data := book.APIGetWorkResponse{}
	err = r.api.Get(fmt.Sprintf(r.getBookDetailURL, key)).Call().Unmarshal(&data)
	if err != nil {
		return
	}

	authors := []author.Author{}
	for _, v := range data.GetAuthorKeys() {
		authors = append(authors, author.Author{Key: v})
	}

	res = book.Book{
		Key:      key,
		Title:    data.Title,
		Subtitle: &data.Subtitle,
		Authors:  authors,
	}
	return
}

func (r repo) GetBookDetail(ctx context.Context, key string) (res book.Book, err error) {
	span, _ := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	res, err = r.getBookDetailFromCache(ctx, key)
	if (err == nil) ||
		(err != nil && err != &errors.DataNotFound) {
		return
	}

	res, err = r.getBookDetailFromCloud(ctx, key)
	if err != nil {
		return
	}

	r.cache(ctx, key, res)
	return
}
