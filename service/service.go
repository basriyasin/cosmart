package service

import (
	"sync"

	"gitlab.com/basriyasin/cosmart/config"
	"gitlab.com/basriyasin/cosmart/pkg/api"

	// domain
	authorDomain "gitlab.com/basriyasin/cosmart/domain/author"
	bookDomain "gitlab.com/basriyasin/cosmart/domain/book"
	scheduleDomain "gitlab.com/basriyasin/cosmart/domain/schedule"
	storageDomain "gitlab.com/basriyasin/cosmart/domain/storage"

	// repository
	authorRepository "gitlab.com/basriyasin/cosmart/repository/author"
	bookRepository "gitlab.com/basriyasin/cosmart/repository/book"
	scheduleRepository "gitlab.com/basriyasin/cosmart/repository/schedule"

	// usecase
	bookUsecase "gitlab.com/basriyasin/cosmart/usecase/book"
	scheduleUsecase "gitlab.com/basriyasin/cosmart/usecase/schedule"
)

var (
	storage    storageDomain.Storage
	httpClient api.APIClient

	bookRepo     bookDomain.BookRepo
	authorRepo   authorDomain.AuthorRepo
	scheduleRepo scheduleDomain.ScheduleRepo

	BookUsecase     bookDomain.BookUsecase
	ScheduleUsecase scheduleDomain.ScheduleUsecase
)

func Init(c config.Config) {
	initResource()
	initRepository(c)
	initUsecase(c)
}

func initResource() {
	storage = &sync.Map{}
	httpClient = api.New()
}

func initRepository(c config.Config) {
	bookRepo = bookRepository.New(c.OpenLibrary, httpClient, storage)
	authorRepo = authorRepository.New(c.OpenLibrary, httpClient, storage)
	scheduleRepo = scheduleRepository.New(storage)
}

func initUsecase(c config.Config) {
	BookUsecase = bookUsecase.New(bookRepo)
	ScheduleUsecase = scheduleUsecase.New(bookRepo, authorRepo, scheduleRepo)
}
