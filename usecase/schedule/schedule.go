package schedule

import (
	"context"

	"gitlab.com/basriyasin/cosmart/domain/author"
	"gitlab.com/basriyasin/cosmart/domain/book"
	"gitlab.com/basriyasin/cosmart/domain/schedule"
	"gitlab.com/basriyasin/cosmart/pkg/tracer"
)

type usecase struct {
	book     book.BookRepo
	author   author.AuthorRepo
	schedule schedule.ScheduleRepo
}

func New(
	book book.BookRepo,
	author author.AuthorRepo,
	schedule schedule.ScheduleRepo,
) schedule.ScheduleUsecase {
	return &usecase{
		book:     book,
		author:   author,
		schedule: schedule,
	}
}

func (u usecase) RequestPickup(ctx context.Context, req schedule.RequestPickup) (err error) {
	span, ctx := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	book, err := u.book.GetBookDetail(ctx, req.BookKey)
	if err != nil {
		return
	}

	authors := []author.Author{}
	for _, v := range book.Authors {
		data, err := u.author.GetAuthor(ctx, v.Key)
		if err != nil {
			continue
		}
		authors = append(authors, data)
	}
	book.Authors = authors

	return u.schedule.Insert(ctx, schedule.Schedule{
		PickupTime: req.PickupTime,
		Book:       book,
	})
}

func (u usecase) List(ctx context.Context) (res []schedule.Schedule, err error) {
	span, ctx := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	return u.schedule.List(ctx)
}
