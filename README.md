# Cosmart

## Requirement
- Go (develop using go version go1.19.3 darwin/amd64)
- Nginx (optional)


## Setup
- link `files/etc/nginx/nginx.conf` to your nginx sites-enable folder
```sh
ln -s files/etc/nginx/nginx.conf /etc/nginx/sites-enable/hypefast.conf
```
or
```sh
ln -s files/etc/nginx/nginx.conf /usr/local/etc/nginx/servers/hypefast.conf
```
- restart your nginx
```sh
sudo systemctl restart nginx
```
or
```sh
nginx -s stop && nginx
```
- update config on `files/etc/hypefast/hypefast.development.conf`
```yaml
server:
    http:
        # Don't forget update nginx conf if you decide to run on other port
        address      : :8081 
```
- import postman collection `hypefast.postman_collection.json`
- run the service
```sh
go run cmd/http/main.go
```
or
```sh
make 
```
or
```sh
make run
```

## Limitation
- all book status will always available and ignore `availability` field from get subeject response
- openlibrary.org get by subject response will be limit as
```json
{
  "key": "/subjects/anime",
  "name": "anime",
  "subject_type": "subject",
  "work_count": 22,
  "works": [
    {
      "key": "/works/OL8699352W",
      "title": "Full moon O Sagashite",
      "edition_count": 6,
      "authors": [
        {
          "key": "/authors/OL2944005A",
          "name": "Arina Tanemura"
        }
      ]
    }
 ]
}
```


## Features
### Get book by subject
- Method: `GET`
- Path: `/list/{subject}`
- Example: `/list/love`
- Response:
```json
{
    "request_id": "55cea888-16e9-44b6-859f-504bf5361bbb",
    "data": [
        {
            "key": "OL8699352W",
            "title": "Full moon O Sagashite",
            "edition_count": 6,
            "authors": [
                {
                    "key": "OL2944005A",
                    "name": "Arina Tanemura"
                }
            ]
        }
        ...
    ]
}
```
- Error:
```json
{
    "request_id": "55cea888-16e9-44b6-859f-504bf5361bbb",
    "code": "41000",
    "reason": "data not found"
}
```


### Submit book pickup schedule
- Method: `POST`
- Path: `/pickup`
- Request:
```json
{
    "book_key": "OL8699352W",            (required)
    "pickup_time": "2022-12-31 14:30:00" (required)
}
```
- Response:
```json
{
    "request_id": "55cea888-16e9-44b6-859f-504bf5361bbb",
    "data": "OK"
}
```
- Error:
```json
{
    "request_id": "55cea888-16e9-44b6-859f-504bf5361bbb",
    "code": "41000",
    "reason": "data not found"
}
```


### Get pickup list
- Method: `GET`
- Path: `/pickup`
- Response:
```json
{
    "request_id": "55cea888-16e9-44b6-859f-504bf5361bbb",
    "data": [
        {
            "pickup_time": "2022-12-31 14:30"
            "subtitle": "Vol #6",
            "title": "Full moon O Sagashite",
            "authors": [
                {
                    "key": "OL2944005A",
                    "name": "Arina Tanemura"
                }
            ]
        }
        ...
    ]
}
```
