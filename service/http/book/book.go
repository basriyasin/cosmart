package book

import (
	"gitlab.com/basriyasin/cosmart/pkg/errors"
	"gitlab.com/basriyasin/cosmart/pkg/http"
	"gitlab.com/basriyasin/cosmart/pkg/tracer"
	"gitlab.com/basriyasin/cosmart/service"
)

func Search(r http.HttpContext) (interface{}, error) {
	span, ctx := tracer.StartSpanFromContext(r.Request().Context())
	defer span.Finish()

	subject := r.Vars()["subject"]
	if subject == "" {
		return nil, &errors.BadRequest
	}

	return service.BookUsecase.Search(ctx, subject)
}
