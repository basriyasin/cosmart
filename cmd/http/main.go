package main

import (
	"gitlab.com/basriyasin/cosmart/config"
	"gitlab.com/basriyasin/cosmart/pkg/tracer"
	"gitlab.com/basriyasin/cosmart/pkg/validator"
	"gitlab.com/basriyasin/cosmart/service"
	"gitlab.com/basriyasin/cosmart/service/http"
)

func main() {
	cfg := config.Get()
	tracer.InitJaeger(cfg.AppName, cfg.Server.HTTP.Jeager)
	validator.Init()

	service.Init(cfg)
	server := http.New(&cfg.Server.HTTP)
	server.Run()
}
