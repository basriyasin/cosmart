package schedule

import (
	"context"
	"sync"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/basriyasin/cosmart/domain/schedule"
	"gitlab.com/basriyasin/cosmart/domain/storage"
)

func TestInsert(t *testing.T) {
	var (
		any         = gomock.Any()
		ctrl        = gomock.NewController(t)
		mockStorage = storage.NewMockStorage(ctrl)
		r           = New(mockStorage)
	)
	defer ctrl.Finish()

	test := []struct {
		name    string
		mock    func()
		wantErr bool
	}{
		{
			name:    "success",
			wantErr: false,
			mock: func() {
				mockStorage.EXPECT().Store(any, any)
			},
		},
	}

	for _, tt := range test {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()

			err := r.Insert(context.Background(), schedule.Schedule{})
			if (err != nil) != tt.wantErr {
				t.Errorf("Insert wantErr:%v but got: %v", tt.wantErr, err)
			}
		})
	}
}

func TestList(t *testing.T) {
	var (
		mockStorage = &sync.Map{}
		r           = New(mockStorage)
	)

	for i := 0; i < 10; i++ {
		mockStorage.Store(i, schedule.Schedule{})
	}

	test := []struct {
		name       string
		mock       func()
		wantErr    bool
		dataLength int
	}{
		{
			name:       "success",
			wantErr:    false,
			mock:       func() {},
			dataLength: 10,
		},
	}

	for _, tt := range test {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()

			data, err := r.List(context.Background())
			if (err != nil) != tt.wantErr {
				t.Errorf("List wantErr:%v but got: %v", tt.wantErr, err)
			}

			assert.Equal(t, tt.dataLength, len(data))
		})
	}
}
