package book

import (
	"gitlab.com/basriyasin/cosmart/config"
	"gitlab.com/basriyasin/cosmart/domain/book"
	"gitlab.com/basriyasin/cosmart/domain/storage"
	"gitlab.com/basriyasin/cosmart/pkg/api"
)

type repo struct {
	api              api.APIClient
	storage          storage.Storage
	getBookListURL   string
	getBookDetailURL string
}

func New(
	config config.OpenLibrary,
	api api.APIClient,
	storage storage.Storage,
) book.BookRepo {
	return &repo{
		api:              api,
		storage:          storage,
		getBookListURL:   config.BaseURL + config.BookList,
		getBookDetailURL: config.BaseURL + config.BookDetail,
	}
}
