package book

import (
	"context"

	"gitlab.com/basriyasin/cosmart/domain/author"
)

type (
	BookRepo interface {
		GetBooks(ctx context.Context, subject string) ([]Book, error)
		GetBookDetail(ctx context.Context, key string) (Book, error)
	}

	BookUsecase interface {
		Search(ctx context.Context, subject string) (res []Book, err error)
	}

	Book struct {
		Key      string          `json:"key"`
		Title    string          `json:"title"`
		Subtitle *string         `json:"subtitle"`
		Authors  []author.Author `json:"authors"`
	}
)
