package errors

import (
	"errors"
)

func New(args ...interface{}) *Error {
	err := &Error{}

	for _, arg := range args {
		switch v := arg.(type) {
		case Error:
			err = &v
		case *Error:
			err = v
		case string:
			err.Err = errors.New(arg.(string))
		case error:
			err.Err = arg.(error)
		default:
			err.Err = errors.New("unknown error")
		}
	}

	if err.Err == nil {
		err.Err = errors.New("unknown error")
		if len(err.Reason) > 0 {
			err.Err = errors.New(err.Reason)
		}
	}

	return err
}

func NewFromTemplate(err Error, messages ...interface{}) *Error {
	msg := []string{}
	for _, message := range messages {
		switch m := message.(type) {
		case string:
			msg = []string{m}
		case error:
			msg = []string{m.Error()}
		case []string:
			msg = append(msg, m...)
		}
	}

	err.Messages = msg
	return &err
}
