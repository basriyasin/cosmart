package errors

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewFromTemplate(t *testing.T) {
	tests := []struct {
		name  string
		param *Error
		want  string
	}{
		{
			name:  "string param",
			want:  "error string",
			param: NewFromTemplate(BadRequest, "error string"),
		},
		{
			name:  "built-in error param",
			want:  "built-in error",
			param: NewFromTemplate(BadRequest, errors.New("built-in error")),
		},
		{
			name:  "built-in error param",
			want:  "slice string error",
			param: NewFromTemplate(BadRequest, []string{"slice string error"}),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := tt.param
			assert.Equal(t, tt.want, e.Messages[0])
		})
	}
}

func TestError(t *testing.T) {
	tests := []struct {
		name  string
		param *Error
		want  string
	}{
		{
			name:  "nil *Error",
			want:  "",
			param: nil,
		},
		{
			name:  "nil *Error.Err",
			want:  "{}",
			param: &Error{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			e := tt.param
			assert.Equal(t, tt.want, e.Error())
		})
	}
}
