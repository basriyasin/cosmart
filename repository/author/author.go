package author

import (
	"context"
	"fmt"

	"github.com/opentracing/opentracing-go/log"
	"gitlab.com/basriyasin/cosmart/config"
	"gitlab.com/basriyasin/cosmart/domain/author"
	"gitlab.com/basriyasin/cosmart/domain/storage"
	"gitlab.com/basriyasin/cosmart/pkg/api"
	"gitlab.com/basriyasin/cosmart/pkg/errors"
	"gitlab.com/basriyasin/cosmart/pkg/tracer"
	"gitlab.com/basriyasin/cosmart/pkg/util"
)

type repo struct {
	api          api.APIClient
	storage      storage.Storage
	getAuthorURL string
}

func New(
	config config.OpenLibrary,
	api api.APIClient,
	storage storage.Storage,
) author.AuthorRepo {
	return &repo{
		api:          api,
		storage:      storage,
		getAuthorURL: config.BaseURL + config.Author,
	}
}

func (r repo) cacheAuthor(ctx context.Context, key string, data author.Author) {
	span, _ := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	r.storage.Store(key, data)
}

func (r repo) getAuthorFromCache(ctx context.Context, key string) (res author.Author, err error) {
	span, _ := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	data, ok := r.storage.Load(key)
	if !ok {
		return res, &errors.DataNotFound
	}

	res, ok = data.(author.Author)
	if !ok {
		log.Error(fmt.Errorf("could not cast data into author.Author with data:%+v", data))
		return res, &errors.InternalServerError
	}

	return
}

func (r repo) getAuthorFromCloud(ctx context.Context, key string) (res author.Author, err error) {
	span, _ := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	err = r.api.Get(fmt.Sprintf(r.getAuthorURL, key)).Call().Unmarshal(&res)
	if err != nil {
		return
	}

	res.Key = util.GetOpenLibraryKey(res.Key)
	return
}

func (r repo) GetAuthor(ctx context.Context, key string) (res author.Author, err error) {
	span, ctx := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	res, err = r.getAuthorFromCache(ctx, key)
	if (err == nil) ||
		(err != nil && err != &errors.DataNotFound) {
		return
	}

	res, err = r.getAuthorFromCloud(ctx, key)
	if err != nil {
		return
	}

	r.cacheAuthor(ctx, key, res)

	return
}
