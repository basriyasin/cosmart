// Code generated by MockGen. DO NOT EDIT.
// Source: pkg/api/client.go

// Package api is a generated GoMock package.
package api

import (
	http "net/http"
	url "net/url"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockClient is a mock of Client interface.
type MockClient struct {
	ctrl     *gomock.Controller
	recorder *MockClientMockRecorder
}

// MockClientMockRecorder is the mock recorder for MockClient.
type MockClientMockRecorder struct {
	mock *MockClient
}

// NewMockClient creates a new mock instance.
func NewMockClient(ctrl *gomock.Controller) *MockClient {
	mock := &MockClient{ctrl: ctrl}
	mock.recorder = &MockClientMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockClient) EXPECT() *MockClientMockRecorder {
	return m.recorder
}

// AddHeader mocks base method.
func (m *MockClient) AddHeader(key, val string) Client {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "AddHeader", key, val)
	ret0, _ := ret[0].(Client)
	return ret0
}

// AddHeader indicates an expected call of AddHeader.
func (mr *MockClientMockRecorder) AddHeader(key, val interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AddHeader", reflect.TypeOf((*MockClient)(nil).AddHeader), key, val)
}

// AddHeaders mocks base method.
func (m *MockClient) AddHeaders(header map[string]string) Client {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "AddHeaders", header)
	ret0, _ := ret[0].(Client)
	return ret0
}

// AddHeaders indicates an expected call of AddHeaders.
func (mr *MockClientMockRecorder) AddHeaders(header interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AddHeaders", reflect.TypeOf((*MockClient)(nil).AddHeaders), header)
}

// Call mocks base method.
func (m *MockClient) Call() Client {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Call")
	ret0, _ := ret[0].(Client)
	return ret0
}

// Call indicates an expected call of Call.
func (mr *MockClientMockRecorder) Call() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Call", reflect.TypeOf((*MockClient)(nil).Call))
}

// Error mocks base method.
func (m *MockClient) Error() error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Error")
	ret0, _ := ret[0].(error)
	return ret0
}

// Error indicates an expected call of Error.
func (mr *MockClientMockRecorder) Error() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Error", reflect.TypeOf((*MockClient)(nil).Error))
}

// Insecure mocks base method.
func (m *MockClient) Insecure() Client {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Insecure")
	ret0, _ := ret[0].(Client)
	return ret0
}

// Insecure indicates an expected call of Insecure.
func (mr *MockClientMockRecorder) Insecure() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Insecure", reflect.TypeOf((*MockClient)(nil).Insecure))
}

// Response mocks base method.
func (m *MockClient) Response() (*http.Response, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Response")
	ret0, _ := ret[0].(*http.Response)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Response indicates an expected call of Response.
func (mr *MockClientMockRecorder) Response() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Response", reflect.TypeOf((*MockClient)(nil).Response))
}

// ResponseBody mocks base method.
func (m *MockClient) ResponseBody() ([]byte, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ResponseBody")
	ret0, _ := ret[0].([]byte)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// ResponseBody indicates an expected call of ResponseBody.
func (mr *MockClientMockRecorder) ResponseBody() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ResponseBody", reflect.TypeOf((*MockClient)(nil).ResponseBody))
}

// Unmarshal mocks base method.
func (m *MockClient) Unmarshal(v interface{}) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Unmarshal", v)
	ret0, _ := ret[0].(error)
	return ret0
}

// Unmarshal indicates an expected call of Unmarshal.
func (mr *MockClientMockRecorder) Unmarshal(v interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Unmarshal", reflect.TypeOf((*MockClient)(nil).Unmarshal), v)
}

// MockAPIClient is a mock of APIClient interface.
type MockAPIClient struct {
	ctrl     *gomock.Controller
	recorder *MockAPIClientMockRecorder
}

// MockAPIClientMockRecorder is the mock recorder for MockAPIClient.
type MockAPIClientMockRecorder struct {
	mock *MockAPIClient
}

// NewMockAPIClient creates a new mock instance.
func NewMockAPIClient(ctrl *gomock.Controller) *MockAPIClient {
	mock := &MockAPIClient{ctrl: ctrl}
	mock.recorder = &MockAPIClientMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockAPIClient) EXPECT() *MockAPIClientMockRecorder {
	return m.recorder
}

// Delete mocks base method.
func (m *MockAPIClient) Delete(url string, body interface{}) (Client, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Delete", url, body)
	ret0, _ := ret[0].(Client)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Delete indicates an expected call of Delete.
func (mr *MockAPIClientMockRecorder) Delete(url, body interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Delete", reflect.TypeOf((*MockAPIClient)(nil).Delete), url, body)
}

// Do mocks base method.
func (m *MockAPIClient) Do(client *http.Client, req *http.Request) (*http.Response, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Do", client, req)
	ret0, _ := ret[0].(*http.Response)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Do indicates an expected call of Do.
func (mr *MockAPIClientMockRecorder) Do(client, req interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Do", reflect.TypeOf((*MockAPIClient)(nil).Do), client, req)
}

// Get mocks base method.
func (m *MockAPIClient) Get(url string) Client {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Get", url)
	ret0, _ := ret[0].(Client)
	return ret0
}

// Get indicates an expected call of Get.
func (mr *MockAPIClientMockRecorder) Get(url interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Get", reflect.TypeOf((*MockAPIClient)(nil).Get), url)
}

// Patch mocks base method.
func (m *MockAPIClient) Patch(url string, body interface{}) (Client, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Patch", url, body)
	ret0, _ := ret[0].(Client)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Patch indicates an expected call of Patch.
func (mr *MockAPIClientMockRecorder) Patch(url, body interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Patch", reflect.TypeOf((*MockAPIClient)(nil).Patch), url, body)
}

// Post mocks base method.
func (m *MockAPIClient) Post(url string, body interface{}) (Client, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Post", url, body)
	ret0, _ := ret[0].(Client)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Post indicates an expected call of Post.
func (mr *MockAPIClientMockRecorder) Post(url, body interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Post", reflect.TypeOf((*MockAPIClient)(nil).Post), url, body)
}

// PostForm mocks base method.
func (m *MockAPIClient) PostForm(url string, form url.Values) (Client, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "PostForm", url, form)
	ret0, _ := ret[0].(Client)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// PostForm indicates an expected call of PostForm.
func (mr *MockAPIClientMockRecorder) PostForm(url, form interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "PostForm", reflect.TypeOf((*MockAPIClient)(nil).PostForm), url, form)
}

// Put mocks base method.
func (m *MockAPIClient) Put(url string, body interface{}) (Client, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Put", url, body)
	ret0, _ := ret[0].(Client)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Put indicates an expected call of Put.
func (mr *MockAPIClientMockRecorder) Put(url, body interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Put", reflect.TypeOf((*MockAPIClient)(nil).Put), url, body)
}
