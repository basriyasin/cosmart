package healthcheck

import (
	"gitlab.com/basriyasin/cosmart/pkg/http"
	"gitlab.com/basriyasin/cosmart/pkg/tracer"
)

func Ping(ctx http.HttpContext) (interface{}, error) {
	span, _ := tracer.StartSpanFromContext(ctx.Request().Context())
	defer span.Finish()

	return "pong", nil
}
