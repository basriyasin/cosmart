package book

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/basriyasin/cosmart/domain/book"
	"gitlab.com/basriyasin/cosmart/pkg/errors"
)

func TestGetBooks(t *testing.T) {
	var (
		any      = gomock.Any()
		ctrl     = gomock.NewController(t)
		mockBook = book.NewMockBookRepo(ctrl)
		r        = New(mockBook)
	)
	defer ctrl.Finish()

	test := []struct {
		name    string
		mock    func()
		wantErr bool
	}{
		{
			name:    "failed",
			wantErr: true,
			mock: func() {
				mockBook.EXPECT().GetBooks(any, any).Return(nil, &errors.InternalServerError)
			},
		},
		{
			name:    "success",
			wantErr: false,
			mock: func() {
				mockBook.EXPECT().GetBooks(any, any).Return(nil, nil)
			},
		},
	}

	for _, tt := range test {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()

			_, err := r.Search(context.Background(), "")
			if (err != nil) != tt.wantErr {
				t.Errorf("Search wantErr:%v but got: %v", tt.wantErr, err)
			}
		})
	}
}
