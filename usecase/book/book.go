package book

import (
	"context"

	"gitlab.com/basriyasin/cosmart/domain/book"
	"gitlab.com/basriyasin/cosmart/pkg/tracer"
)

type usecase struct {
	book book.BookRepo
}

func New(
	book book.BookRepo,
) book.BookUsecase {
	return &usecase{
		book: book,
	}
}

func (u usecase) Search(ctx context.Context, subject string) (res []book.Book, err error) {
	span, ctx := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	return u.book.GetBooks(ctx, subject)
}
