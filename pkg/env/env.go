package env

import (
	"os"
	"runtime"
)

type ENV = string

const (
	DevelopmentEnv = "development"
	StagingEnv     = "staging"
	ProductionEnv  = "production"
	DefaultAppName = "go-app"
)

var (
	EnvName   = "{COSMART_ENV}"
	goVersion string
)

func init() {
	goVersion = runtime.Version()
}

func ServiceEnv() ENV {
	e := os.Getenv(EnvName)
	if e == "" {
		e = DevelopmentEnv
	}
	return e
}

func GoVersion() string {
	return goVersion
}

func IsDevelopment() bool {
	return ServiceEnv() == DevelopmentEnv
}

func IsStaging() bool {
	return ServiceEnv() == StagingEnv
}

func IsProduction() bool {
	return ServiceEnv() == ProductionEnv
}
