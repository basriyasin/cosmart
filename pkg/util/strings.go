package util

import (
	"strings"
)

func GetOpenLibraryKey(s string) string {
	el := strings.Split(s, "/")
	return el[len(el)-1]
}
