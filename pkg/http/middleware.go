package http

type (
	MiddlewareFunc func(HttpContext) error

	Middleware struct {
		handler map[int]MiddlewareFunc
	}
)

func InitMiddeware() *Middleware {
	return &Middleware{
		handler: map[int]MiddlewareFunc{},
	}

}

func (m *Middleware) Register(id int, handler MiddlewareFunc) {
	m.handler[id] = handler
}

func (m Middleware) Get(middleware int) MiddlewareFunc {
	h, ok := m.handler[middleware]
	if !ok {
		panic("middleware not registered")
	}

	return h
}

func (m Middleware) Gets(middleware ...int) []MiddlewareFunc {
	middlewares := make([]MiddlewareFunc, len(middleware))
	for _, v := range middleware {
		middlewares = append(middlewares, m.Get(v))
	}

	return middlewares
}

func MiddlewareFilter(ctx HttpContext, middleware ...MiddlewareFunc) (err error) {
	for _, m := range middleware {
		err = m(ctx)
		if err != nil {
			return err
		}
	}

	return err
}
