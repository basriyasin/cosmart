package storage

type (
	Storage interface {
		Load(key any) (value any, ok bool)
		Store(key, value any)
		LoadOrStore(key, value any) (actual any, loaded bool)
		LoadAndDelete(key any) (value any, loaded bool)
		Delete(key any)
		Range(f func(key, value any) bool)
	}
)
