package config

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/basriyasin/cosmart/pkg/env"
	ini "gopkg.in/ini.v1"
	yaml "gopkg.in/yaml.v2"
)

var (
	ErrNoFileFound = errors.New("no config file found")
)

func Read(dest interface{}, paths ...string) error {
	for _, path := range paths {
		path = replacePathByEnv(path)

		if _, err := os.Stat(path); os.IsNotExist(err) {
			continue
		}

		ext := filepath.Ext(path)
		f, err := os.Open(path)
		if err != nil {
			return err
		}
		content, err := ioutil.ReadAll(f)
		if err != nil {
			return err
		}
		switch {
		case ext == ".ini":
			return loadIniConfig(dest, path)
		case ext == ".yaml" || ext == ".yml":
			return yaml.Unmarshal(content, dest)
		case ext == ".json":
			return json.Unmarshal(content, dest)
		}
	}
	return ErrNoFileFound
}

func loadIniConfig(dest interface{}, path string) error {
	f, err := ini.Load(path)
	if err != nil {
		return err
	}
	return f.MapTo(dest)
}

func replacePathByEnv(path string) string {
	return strings.ReplaceAll(path, env.EnvName, env.ServiceEnv())
}
