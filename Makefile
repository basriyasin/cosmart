run:
	@go run cmd/http/main.go

pkgs := $(shell go list ./... | grep -v vendor | grep -v mock)
test:
	@go test -cover $(pkgs)
