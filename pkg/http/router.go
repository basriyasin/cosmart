package http

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"gitlab.com/basriyasin/cosmart/pkg/tracer"
)

type Router struct {
	router *mux.Router
}

type HandlerFunc func(ctx HttpContext) (interface{}, error)

func (r *Router) Use(f func(http.Handler) http.Handler) {
	r.router.Use(f)
}

func (r *Router) HandleFunc(path string, handler HandlerFunc, methods ...string) {
	for _, method := range methods {
		route := r.router.Methods(method).Path(path)
		route.HandlerFunc(wrapHandler(handler))
	}
}

func (r *Router) HandleFuncPrefix(path string, handler HandlerFunc, methods ...string) {
	for _, method := range methods {
		route := r.router.Methods(method).PathPrefix(path)
		route.HandlerFunc(wrapHandler(handler))
	}
}

func (r *Router) HandleFile(path string, dirs ...string) {
	for _, dir := range dirs {
		if _, err := os.Stat(dir); os.IsNotExist(err) {
			log.Printf("Can't find directory %s, skipping.. \n", dir)
			continue
		}
		r.router.PathPrefix(path).
			Handler(
				http.StripPrefix(path,
					http.FileServer(http.Dir(dir)),
				),
			)
		log.Printf("Serving file %s on path %s \n", dir, path)
		break
	}
}

func (r *Router) Get(path string, handler HandlerFunc, middleware ...MiddlewareFunc) {
	route := r.router.Methods(http.MethodGet).Path(path)
	route.HandlerFunc(wrapHandler(handler, middleware...))
}

func (r *Router) Post(path string, handler HandlerFunc, middleware ...MiddlewareFunc) {
	route := r.router.Methods(http.MethodPost).Path(path)
	route.HandlerFunc(wrapHandler(handler, middleware...))
}

func (r *Router) Patch(path string, handler HandlerFunc, middleware ...MiddlewareFunc) {
	route := r.router.Methods(http.MethodPatch).Path(path)
	route.HandlerFunc(wrapHandler(handler, middleware...))
}

func (r *Router) Delete(path string, handler HandlerFunc, middleware ...MiddlewareFunc) {
	route := r.router.Methods(http.MethodDelete).Path(path)
	route.HandlerFunc(wrapHandler(handler, middleware...))
}

func wrapHandler(handler HandlerFunc, middleware ...MiddlewareFunc) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		span, rctx := tracer.StartSpanFromContext(r.Context())
		defer span.Finish()
		r = r.WithContext(rctx)

		var (
			ctx  = newContext(w, r)
			err  error
			data interface{}
		)

		err = MiddlewareFilter(ctx, middleware...)
		if err == nil {
			data, err = handler(ctx)
			if err == nil {
				ctx.JSON(http.StatusOK, HttpResponse{
					RequestID: ctx.RequestID(),
					Data:      data,
				})
				return
			}
		}

		ctx.HandleErrorJSON(err)
	}
}
