package schedule

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/basriyasin/cosmart/domain/author"
	"gitlab.com/basriyasin/cosmart/domain/book"
	"gitlab.com/basriyasin/cosmart/domain/schedule"
	"gitlab.com/basriyasin/cosmart/pkg/errors"
)

func TestRequestPickup(t *testing.T) {
	var (
		any          = gomock.Any()
		ctrl         = gomock.NewController(t)
		mockBook     = book.NewMockBookRepo(ctrl)
		mockAuthor   = author.NewMockAuthorRepo(ctrl)
		mockSchedule = schedule.NewMockScheduleRepo(ctrl)
		r            = New(mockBook, mockAuthor, mockSchedule)
	)
	defer ctrl.Finish()

	test := []struct {
		name    string
		mock    func()
		wantErr bool
	}{
		{
			name:    "failed get book detail",
			wantErr: true,
			mock: func() {
				mockBook.EXPECT().GetBookDetail(any, any).Return(book.Book{}, &errors.InternalServerError)
			},
		},
		{
			name:    "failed insert schedule and get some author",
			wantErr: true,
			mock: func() {
				mockBook.EXPECT().GetBookDetail(any, any).Return(book.Book{Authors: []author.Author{{}, {}}}, nil)
				mockAuthor.EXPECT().GetAuthor(any, any).Return(author.Author{}, nil)
				mockAuthor.EXPECT().GetAuthor(any, any).Return(author.Author{}, &errors.DataNotFound)
				mockSchedule.EXPECT().Insert(any, any).Return(&errors.InternalServerError)
			},
		},
		{
			name:    "success",
			wantErr: false,
			mock: func() {
				mockBook.EXPECT().GetBookDetail(any, any).Return(book.Book{Authors: []author.Author{{}, {}}}, nil)
				mockAuthor.EXPECT().GetAuthor(any, any).Return(author.Author{}, nil)
				mockAuthor.EXPECT().GetAuthor(any, any).Return(author.Author{}, &errors.DataNotFound)
				mockSchedule.EXPECT().Insert(any, any).Return(nil)
			},
		},
	}

	for _, tt := range test {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()

			err := r.RequestPickup(context.Background(), schedule.RequestPickup{})
			if (err != nil) != tt.wantErr {
				t.Errorf("RequestPickup wantErr:%v but got: %v", tt.wantErr, err)
			}
		})
	}
}

func TestList(t *testing.T) {
	var (
		any          = gomock.Any()
		ctrl         = gomock.NewController(t)
		mockSchedule = schedule.NewMockScheduleRepo(ctrl)
		r            = New(nil, nil, mockSchedule)
	)
	defer ctrl.Finish()

	test := []struct {
		name    string
		mock    func()
		wantErr bool
	}{
		{
			name:    "failed get book detail",
			wantErr: true,
			mock: func() {
				mockSchedule.EXPECT().List(any).Return(nil, &errors.InternalServerError)
			},
		},
		{
			name:    "success",
			wantErr: false,
			mock: func() {
				mockSchedule.EXPECT().List(any).Return([]schedule.Schedule{}, nil)
			},
		},
	}

	for _, tt := range test {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()

			_, err := r.List(context.Background())
			if (err != nil) != tt.wantErr {
				t.Errorf("List wantErr:%v but got: %v", tt.wantErr, err)
			}
		})
	}
}
