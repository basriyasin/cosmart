package config

import (
	"gitlab.com/basriyasin/cosmart/pkg/tracer"
)

type (
	Config struct {
		AppName     string      `yaml:"app_name"`
		Server      Server      `yaml:"server"`
		OpenLibrary OpenLibrary `yaml:"open_library"`
	}

	Server struct {
		HTTP HTTP `yaml:"http"`
	}

	HTTP struct {
		Address      string        `yaml:"address"`
		WriteTimeout int           `yaml:"write_timeout"`
		ReadTimeout  int           `yaml:"read_timeout"`
		Jeager       tracer.Config `yaml:"jeager"`
	}

	OpenLibrary struct {
		BaseURL    string `yaml:"base_url"`
		Author     string `yaml:"author"`
		BookList   string `yaml:"book_list"`
		BookDetail string `yaml:"book_detail"`
	}
)
