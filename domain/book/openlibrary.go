package book

import "gitlab.com/basriyasin/cosmart/pkg/util"

type (
	// openlibrary.org response /subject/{subject}
	APIGetSubjectResponse struct {
		Works []Book
	}

	// openlibrary.org response /works/{key}
	APIGetWorkResponse struct {
		Subtitle string             `json:"subtitle"`
		Title    string             `json:"title"`
		Author   []APIGetWorkAuthor `json:"authors"`
	}

	APIGetWorkAuthor struct {
		Author struct {
			Key string `json:"key"`
		} `json:"author"`
	}
)

func (a *APIGetWorkResponse) GetAuthorKeys() (keys []string) {
	for _, author := range a.Author {
		keys = append(keys, util.GetOpenLibraryKey(author.Author.Key))
	}

	return keys
}
