package http

import (
	"context"
	"errors"
	"net"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

type ServerHandler interface {
	RegisterHandler(r *Router)
}

type Config struct {
	Address      string        `yaml:"address"       json:"address"`
	WriteTimeout time.Duration `yaml:"write_timeout" json:"write_timeout"`
	ReadTimeout  time.Duration `yaml:"read_timeout"  json:"read_timeout"`
}

func (cfg *Config) CheckDefault() {
	if cfg == nil {
		cfg = &Config{}
	}
	if cfg.Address == "" {
		cfg.Address = ":8080"
	}
}

type Service struct {
	config *Config
	router *Router
	server *http.Server
}

func New(cfg *Config) *Service {
	if cfg == nil {
		cfg = &Config{}
	}
	cfg.CheckDefault()

	r := &Router{
		router: mux.NewRouter(),
	}

	svc := &Service{
		config: cfg,
		server: &http.Server{
			ReadTimeout:  cfg.ReadTimeout,
			WriteTimeout: cfg.WriteTimeout,
		},
		router: r,
	}
	return svc
}

// Config return http service config
func (svc Service) Config() *Config {
	return svc.config
}

// Address of http service
func (svc *Service) Address() string {
	return svc.config.Address
}

// Type of http service
func (svc *Service) Type() string {
	return "http-service"
}

// Server return http service http server
func (svc Service) Server() *http.Server {
	return svc.server
}

// Code returns service code to be used by admin page
func (svc Service) Code() string {
	return "http"
}

// Serve http service
func (svc *Service) Serve(ls net.Listener) error {
	return svc.Server().Serve(ls)
}

// Shutdown http service
func (svc *Service) Shutdown(ctx context.Context) error {
	return svc.Server().Shutdown(ctx)
}

// RegisterService execute services' RegisterHandler
func (svc *Service) RegisterService(services ...ServerHandler) {
	for _, s := range services {
		s.RegisterHandler(svc.router)
	}
	svc.server.Handler = svc.router.router
}

// SetRouterTimeout wrap server handler using http.TimeoutHandler
// Can only be run once
func (svc *Service) SetRouterTimeout(t time.Duration, msg string) error {
	if t == 0 || msg == "" {
		return errors.New("timeout and message cannot be empty")
	}
	svc.server.Handler = http.TimeoutHandler(svc.server.Handler, t, msg)
	return nil
}

// GetHandler return list of handler
func (svc *Service) GetHandler() http.Handler {
	return svc.router.router
}

// GetRouter return the router
func (svc *Service) GetRouter() *Router {
	return svc.router
}
