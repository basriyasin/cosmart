package http

import (
	"context"
	"html/template"
	"io"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/basriyasin/cosmart/pkg/errors"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

// TODO:
// - implement set cookie as cookie container enable all handler add cookie
// - write all cookie from cookie container
type HttpContext interface {
	// Request identifier
	RequestID() string

	// Request returns the underlying request
	Request() *http.Request

	// Writer return the underlying response writer
	Writer() http.ResponseWriter

	// Body returns the request body
	Body() ([]byte, error)

	// HTML renders HTML `*template.Template` with the data provided
	// and write it as response
	HTML(code int, tmpl *template.Template, data interface{}) error

	// JSON write json response with the given http status code
	JSON(code int, data interface{}) error

	// Write raw []byte response with the given http status code
	Write(code int, data []byte) (int, error)

	// Vars returns map of the parsed URL query string or parsed pseudo regex arguments from UR
	Vars() map[string]string

	// DecodeJSONBody json.Unmarshal request body to data
	DecodeJSONBody(data interface{}) error

	// append value to request context
	WithContextValue(key, value interface{})

	// handling error and returning JSON response
	HandleErrorJSON(err error)
}

type HttpResponse struct {
	RequestID string      `json:"request_id,omitempty"`
	Code      string      `json:"code,omitempty"`
	Reason    string      `json:"reason,omitempty"`
	Messages  []string    `json:"messages,omitempty"`
	Data      interface{} `json:"data,omitempty"`
}

type defaultContext struct {
	requestID string
	writer    http.ResponseWriter
	request   *http.Request
	body      []byte
	vars      map[string]string
}

func (c *defaultContext) Writer() http.ResponseWriter {
	return c.writer
}

func (c *defaultContext) Request() *http.Request {
	return c.request
}

func (c *defaultContext) WithContextValue(key, value interface{}) {
	ctx := context.WithValue(c.request.Context(), key, value)
	c.request = c.request.WithContext(ctx)
}

func (c *defaultContext) Body() ([]byte, error) {
	if c.body == nil {
		b, err := io.ReadAll(c.request.Body)
		if err != nil {
			return nil, err
		}
		c.body = b
	}
	return c.body, nil
}

func (c *defaultContext) HTML(code int, tmpl *template.Template, data interface{}) error {
	c.writer.WriteHeader(code)
	c.writer.Header().Set("Content-Type", "text/html")
	return tmpl.Execute(c.writer, data)
}

func (c *defaultContext) JSON(code int, data interface{}) error {
	c.writer.Header().Set("Content-Type", "application/json")
	c.writer.WriteHeader(code)
	return json.NewEncoder(c.writer).Encode(data)
}

func (c *defaultContext) Vars() map[string]string {
	if c.vars == nil {
		c.vars = make(map[string]string)
		c.vars = mux.Vars(c.request)

		// append it with url Query paramss
		query := c.request.URL.Query()
		for k := range query {
			c.vars[k] = query.Get(k)
		}
	}
	return c.vars
}

func (c *defaultContext) Write(code int, resp []byte) (int, error) {
	return c.writer.Write(resp)
}

func newContext(w http.ResponseWriter, r *http.Request) HttpContext {
	return &defaultContext{
		requestID: uuid.NewString(),
		writer:    w,
		request:   r,
	}
}

func (c *defaultContext) DecodeJSONBody(data interface{}) error {
	body, err := c.Body()
	if err != nil {
		return err
	}
	return json.Unmarshal(body, data)
}

func (c *defaultContext) RequestID() string {
	return c.requestID
}

func (c *defaultContext) HandleErrorJSON(err error) {
	errs := errors.New(err)
	res := HttpResponse{
		RequestID: c.RequestID(),
		Code:      errs.Code,
		Reason:    errs.Reason,
		Messages:  errs.Messages,
	}

	status := errors.DefaultError.Status
	if errs.Status > 299 && errs.Status < 600 {
		status = errs.Status
	}

	if errs.Code == "" {
		res.Code = errors.DefaultError.Code
	}

	if errs.Reason == "" {
		res.Reason = http.StatusText(status)
	}

	c.JSON(status, res)
}
