package book

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetAuthorKeys(t *testing.T) {
	createAuthor := func(key ...string) []APIGetWorkAuthor {
		res := []APIGetWorkAuthor{}
		for _, v := range key {
			res = append(res, APIGetWorkAuthor{
				Author: struct {
					Key string "json:\"key\""
				}{v},
			})
		}
		return res
	}

	test := []struct {
		name string
		mock func() []APIGetWorkAuthor
		want []string
	}{
		{
			name: "empty author",
			want: nil,
			mock: func() []APIGetWorkAuthor {
				return []APIGetWorkAuthor{}
			},
		},
		{
			name: "single author",
			want: []string{"basri"},
			mock: func() []APIGetWorkAuthor {
				return createAuthor("basri")
			},
		},
		{
			name: "multiple author",
			want: []string{"basri", "yasin", "x123"},
			mock: func() []APIGetWorkAuthor {
				return createAuthor("basri", "yasin", "x123")
			},
		},
	}

	for _, tt := range test {
		t.Run(tt.name, func(t *testing.T) {
			r := APIGetWorkResponse{
				Author: tt.mock(),
			}

			got := r.GetAuthorKeys()
			assert.Equal(t, tt.want, got)

		})
	}
}
