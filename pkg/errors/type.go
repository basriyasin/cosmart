package errors

import (
	"encoding/json"
	"net/http"
)

var DefaultError = Error{
	Status: http.StatusInternalServerError,
	Code:   "50000",
	Reason: "unknown error",
}

type Error struct {
	Err      error    `json:"err,omitempty"`
	Status   int      `json:"status,omitempty"`
	Code     string   `json:"code,omitempty"`
	Messages []string `json:"messages,omitempty"`
	Reason   string   `json:"reason,omitempty"`
}

func (e *Error) Error() string {
	if e == nil {
		return ""
	}

	return e.String()
}

func (e *Error) String() string {
	b, _ := json.Marshal(e)
	return string(b)
}
