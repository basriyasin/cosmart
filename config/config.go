package config

import (
	"log"
	"sync"

	"gitlab.com/basriyasin/cosmart/pkg/config"
)

var (
	cfg  Config
	once sync.Once

	Paths = []string{
		"/etc/cosmart/cosmart.{COSMART_ENV}.yaml",
		"files/etc/cosmart/cosmart.{COSMART_ENV}.yaml",
	}
)

func Get() Config {
	once.Do(func() {
		err := config.Read(&cfg, Paths...)
		if err != nil {
			log.Fatal(err)
		}
	})

	return cfg
}
