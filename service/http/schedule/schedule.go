package schedule

import (
	"gitlab.com/basriyasin/cosmart/domain/schedule"
	"gitlab.com/basriyasin/cosmart/pkg/errors"
	"gitlab.com/basriyasin/cosmart/pkg/http"
	"gitlab.com/basriyasin/cosmart/pkg/tracer"
	"gitlab.com/basriyasin/cosmart/pkg/validator"
	"gitlab.com/basriyasin/cosmart/service"
)

func Pickup(r http.HttpContext) (interface{}, error) {
	span, ctx := tracer.StartSpanFromContext(r.Request().Context())
	defer span.Finish()

	req := schedule.RequestPickup{}
	err := r.DecodeJSONBody(&req)
	if err != nil {
		return nil, &errors.BadRequest
	}

	err = validator.Validate(req)
	if err != nil {
		return nil, err
	}

	return "OK", service.ScheduleUsecase.RequestPickup(ctx, req)
}

func List(r http.HttpContext) (interface{}, error) {
	span, ctx := tracer.StartSpanFromContext(r.Request().Context())
	defer span.Finish()

	return service.ScheduleUsecase.List(ctx)
}
