package tracer

import (
	"context"
	"runtime"
	"strings"

	opentracing "github.com/opentracing/opentracing-go"
)

func StartSpanFromContext(ctx context.Context, opts ...opentracing.StartSpanOption) (opentracing.Span, context.Context) {
	pc, _, _, _ := runtime.Caller(1)
	operationName := runtime.FuncForPC(pc).Name()[strings.LastIndex(runtime.FuncForPC(pc).Name(), "/")+1:]
	return opentracing.StartSpanFromContext(ctx, operationName, opts...)
}
