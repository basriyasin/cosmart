package util

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetOpenLibraryKey(t *testing.T) {
	test := map[string]string{
		"a":            "a",
		"////b":        "b",
		"/subject/123": "123",
		"/subject/":    "",
	}

	for param, ex := range test {
		assert.Equal(t, ex, GetOpenLibraryKey(param))
	}
}
