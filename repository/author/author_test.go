package author

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"gitlab.com/basriyasin/cosmart/config"
	"gitlab.com/basriyasin/cosmart/domain/author"
	"gitlab.com/basriyasin/cosmart/domain/storage"
	"gitlab.com/basriyasin/cosmart/pkg/api"
	"gitlab.com/basriyasin/cosmart/pkg/errors"
)

func TestGetAuthor(t *testing.T) {
	var (
		any           = gomock.Any()
		ctrl          = gomock.NewController(t)
		mockStorage   = storage.NewMockStorage(ctrl)
		mockApi       = api.NewMockAPIClient(ctrl)
		mockApiClient = api.NewMockClient(ctrl)
		r             = New(config.OpenLibrary{}, mockApi, mockStorage)
	)
	defer ctrl.Finish()

	test := []struct {
		name    string
		mock    func()
		wantErr bool
	}{
		{
			name:    "get from cache error failed cast data to author.Author",
			wantErr: true,
			mock: func() {
				mockStorage.EXPECT().Load(any).Return(r, true)
			},
		},
		{
			name:    "data not found on cache and error get from cloud",
			wantErr: true,
			mock: func() {
				mockStorage.EXPECT().Load(any).Return(nil, false)
				mockApi.EXPECT().Get(any).Return(mockApiClient)
				mockApiClient.EXPECT().Call().Return(mockApiClient)
				mockApiClient.EXPECT().Unmarshal(any).Return(&errors.InternalServerError)
			},
		},
		{
			name:    "data not found on cache and success get from cloud",
			wantErr: false,
			mock: func() {
				mockStorage.EXPECT().Load(any).Return(nil, false)
				mockApi.EXPECT().Get(any).Return(mockApiClient)
				mockApiClient.EXPECT().Call().Return(mockApiClient)
				mockApiClient.EXPECT().Unmarshal(any).Return(nil)

				mockStorage.EXPECT().Store(any, any)
			},
		},
		{
			name:    "data found on cache",
			wantErr: false,
			mock: func() {
				mockStorage.EXPECT().Load(any).Return(author.Author{}, true)
			},
		},
	}

	for _, tt := range test {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock()

			_, err := r.GetAuthor(context.Background(), "")
			if (err != nil) != tt.wantErr {
				t.Errorf("GetAuthor wantErr:%v but got: %v", tt.wantErr, err)
			}
		})
	}
}
