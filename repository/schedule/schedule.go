package schedule

import (
	"context"

	"gitlab.com/basriyasin/cosmart/domain/schedule"
	"gitlab.com/basriyasin/cosmart/domain/storage"
	"gitlab.com/basriyasin/cosmart/pkg/tracer"
)

type repo struct {
	storage storage.Storage
	counter uint64
}

func New(
	storage storage.Storage,
) schedule.ScheduleRepo {
	return &repo{
		storage: storage,
	}
}

func (r *repo) Insert(ctx context.Context, schedule schedule.Schedule) (err error) {
	span, _ := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	r.counter++
	r.storage.Store(r.counter, schedule)
	return
}

func (r repo) List(ctx context.Context) (res []schedule.Schedule, err error) {
	span, _ := tracer.StartSpanFromContext(ctx)
	defer span.Finish()

	r.storage.Range(func(key, value any) bool {
		s, ok := value.(schedule.Schedule)
		if ok {
			res = append(res, s)
		}

		return true
	})

	return
}
