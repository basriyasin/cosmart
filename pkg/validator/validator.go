package validator

import (
	"fmt"
	"net/url"
	"regexp"
	"sync"

	"github.com/go-playground/form"
	"gitlab.com/basriyasin/cosmart/pkg/errors"
	"gopkg.in/go-playground/validator.v9"
)

var validate *validator.Validate
var once sync.Once
var decoder *form.Decoder

func Init() {
	once.Do(func() {
		validate = validator.New()
		validate.SetTagName("val")

		// register regex tag
		validate.RegisterValidation("regex", func(fl validator.FieldLevel) bool {
			matched, err := regexp.MatchString(fl.Param(), fl.Field().String())
			if err != nil || !matched {
				return false
			}
			return true
		})

		validate.RegisterValidation("datetime",
			func(fl validator.FieldLevel) bool {
				rule := `(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})`
				reg := regexp.MustCompile(rule)
				return reg.MatchString(fl.Field().String())
			},
		)

		decoder = form.NewDecoder()
		decoder.SetTagName("json")

	})
}

// ValidateStruct will validate struct based on `validate` tag
func Validate(s interface{}) (err error) {
	err = validate.Struct(s)
	if err == nil {
		return nil
	}

	//replace error message with custom error message
	msg := []string{}
	if verr, ok := err.(validator.ValidationErrors); ok {
		for _, e := range verr {
			switch e.Tag() {
			case "required":
				msg = append(msg, fmt.Sprintf("'%s' is required", e.Field()))
			case "min":
				msg = append(msg, fmt.Sprintf("'%s' values should be minimum %s", e.Field(), e.Param()))
			case "max":
				msg = append(msg, fmt.Sprintf("'%s' values should be maximum %s", e.Field(), e.Param()))
			case "regex":
				msg = append(msg, fmt.Sprintf("'%s' does not match with %s", e.Field(), e.Param()))
			case "enum":
				msg = append(msg, fmt.Sprintf("'%s' is should be in [%s]", e.Field(), e.Param()))
			case "url":
				msg = append(msg, fmt.Sprintf("'%s' should be valid URL", e.Field()))
			case "datetime":
				msg = append(msg, fmt.Sprintf("'%s' should match with format YY-MM-DD hh:mm:ss", e.Field()))
			}
		}

		err = errors.NewFromTemplate(errors.BadRequest, msg)
	}
	return err
}

// Decode url form and url query data
func Decode(i interface{}, v url.Values) error {
	return decoder.Decode(i, v)
}
