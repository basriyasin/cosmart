package author

import "context"

type (
	AuthorRepo interface {
		GetAuthor(ctx context.Context, key string) (Author, error)
	}

	Author struct {
		Key  string `json:"key"`
		Name string `json:"name"`
	}

	// openlibrary.org Get author /authors/{key}
	APIGetAutorResponse struct {
		Name string `json:"name"`
	}
)
